/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 04-Jan-2022, 11:32:02 AM                    ---
 * ----------------------------------------------------------------
 */
package org.hybris.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedNagAccInitialDataConstants
{
	public static final String EXTENSIONNAME = "NagAccinitialdata";
	
	protected GeneratedNagAccInitialDataConstants()
	{
		// private constructor
	}
	
	
}
