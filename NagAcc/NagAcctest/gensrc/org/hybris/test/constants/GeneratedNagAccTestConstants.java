/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 04-Jan-2022, 11:32:02 AM                    ---
 * ----------------------------------------------------------------
 */
package org.hybris.test.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedNagAccTestConstants
{
	public static final String EXTENSIONNAME = "NagAcctest";
	
	protected GeneratedNagAccTestConstants()
	{
		// private constructor
	}
	
	
}
