/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.hybris.test.constants;

/**
 * 
 */
public class NagAccTestConstants extends GeneratedNagAccTestConstants
{

	public static final String EXTENSIONNAME = "NagAcctest";

}
