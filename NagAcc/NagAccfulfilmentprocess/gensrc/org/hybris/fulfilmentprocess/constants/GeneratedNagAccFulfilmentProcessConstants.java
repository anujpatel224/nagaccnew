/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 04-Jan-2022, 11:32:02 AM                    ---
 * ----------------------------------------------------------------
 */
package org.hybris.fulfilmentprocess.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedNagAccFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "NagAccfulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedNagAccFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
