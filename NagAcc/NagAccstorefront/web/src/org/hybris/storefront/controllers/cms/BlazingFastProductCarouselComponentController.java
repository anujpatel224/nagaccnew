package org.hybris.storefront.controllers.cms;

import de.hybris.platform.commercefacades.product.data.ProductData;
import org.hybris.NagAcc.core.model.BlazingFastProductCarouselComponentModel;
import org.hybris.facades.suggestion.CustomProductFacade;
import org.hybris.storefront.controllers.ControllerConstants;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller("BlazingFastProductCarouselComponentController")
@Scope("tenant")
@RequestMapping(value= ControllerConstants.Actions.Cms.BlazingFastProductCarouselComponent)
public class BlazingFastProductCarouselComponentController extends AbstractAcceleratorCMSComponentController<BlazingFastProductCarouselComponentModel>{
    @Resource(name = "customProductFacade")
    private CustomProductFacade customProductFacade;

    @Override
    protected void fillModel(HttpServletRequest request, Model model, BlazingFastProductCarouselComponentModel component) {
        final List<ProductData> productDataList = customProductFacade.getProductWithBlazingFastSpeed();
        model.addAttribute("title", "Blazing Fast Deliverable Products");
        model.addAttribute("productData", productDataList);
    }
}
