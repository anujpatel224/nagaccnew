<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="brandCarouselComponent">

      <div class="carousel__component">
        <div class="carousel__component--headline">${fn:escapeXml(title)}</div>
            <div class="carousel__component--carousel js-owl-carousel js-owl-default">
                  <c:forEach items="${productData}" var="product">

                     <c:url value="${product.url}" var="productUrl"/>

                     <div class="carousel__item">
                        <a href="${productUrl}">
                           <div class="carousel__item--thumb">
                              <product:productPrimaryImage product="${product}" format="product"/>
                           </div>
                           <div class="carousel__item--name">${fn:escapeXml(product.name)}</div>
                           <div class="carousel__item--price"><format:fromPrice priceData="${product.price}"/></div>
                        </a>
                     </div>
                  </c:forEach>
               </div>
            </div>

</div>