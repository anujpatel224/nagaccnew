/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 04-Jan-2022, 3:48:20 PM                     ---
 * ----------------------------------------------------------------
 */
package org.hybris.core.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedNagAccCoreConstants
{
	public static final String EXTENSIONNAME = "NagAcccore";
	public static class TC
	{
		public static final String APPARELPRODUCT = "ApparelProduct".intern();
		public static final String APPARELSIZEVARIANTPRODUCT = "ApparelSizeVariantProduct".intern();
		public static final String APPARELSTYLEVARIANTPRODUCT = "ApparelStyleVariantProduct".intern();
		public static final String BLAZINGFASTPRODUCTCAROUSELCOMPONENT = "BlazingFastProductCarouselComponent".intern();
		public static final String DELIVERYSPEED = "DeliverySpeed".intern();
		public static final String ELECTRONICSCOLORVARIANTPRODUCT = "ElectronicsColorVariantProduct".intern();
		public static final String LOGISTICS = "Logistics".intern();
		public static final String SWATCHCOLORENUM = "SwatchColorEnum".intern();
	}
	public static class Attributes
	{
		public static class AbstractOrder
		{
			public static final String DELIVERYSPEED = "deliverySpeed".intern();
		}
		public static class Order
		{
			public static final String LOGISTICS = "Logistics".intern();
		}
		public static class Product
		{
			public static final String DELIVERYSPEED = "deliverySpeed".intern();
			public static final String LOGISTICS = "logistics".intern();
		}
	}
	public static class Enumerations
	{
		public static class DeliverySpeed
		{
			public static final String BLAZINGFAST = "BlazingFast".intern();
			public static final String EXPRESS = "Express".intern();
			public static final String STANDARD = "Standard".intern();
			public static final String RELAXED = "Relaxed".intern();
		}
		public static class SwatchColorEnum
		{
			public static final String BLACK = "BLACK".intern();
			public static final String BLUE = "BLUE".intern();
			public static final String BROWN = "BROWN".intern();
			public static final String GREEN = "GREEN".intern();
			public static final String GREY = "GREY".intern();
			public static final String ORANGE = "ORANGE".intern();
			public static final String PINK = "PINK".intern();
			public static final String PURPLE = "PURPLE".intern();
			public static final String RED = "RED".intern();
			public static final String SILVER = "SILVER".intern();
			public static final String WHITE = "WHITE".intern();
			public static final String YELLOW = "YELLOW".intern();
		}
	}
	public static class Relations
	{
		public static final String ORDER2LOGISTICSRELATION = "Order2LogisticsRelation".intern();
	}
	
	protected GeneratedNagAccCoreConstants()
	{
		// private constructor
	}
	
	
}
