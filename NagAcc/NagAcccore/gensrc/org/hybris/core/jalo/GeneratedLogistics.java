/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 04-Jan-2022, 3:48:20 PM                     ---
 * ----------------------------------------------------------------
 */
package org.hybris.core.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.hybris.core.constants.NagAccCoreConstants;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem Logistics}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedLogistics extends GenericItem
{
	/** Qualifier of the <code>Logistics.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>Logistics.companyName</code> attribute **/
	public static final String COMPANYNAME = "companyName";
	/** Qualifier of the <code>Logistics.supportsBlazingFast</code> attribute **/
	public static final String SUPPORTSBLAZINGFAST = "supportsBlazingFast";
	/** Qualifier of the <code>Logistics.activeOrders</code> attribute **/
	public static final String ACTIVEORDERS = "activeOrders";
	/** Qualifier of the <code>Logistics.OrderPOS</code> attribute **/
	public static final String ORDERPOS = "OrderPOS";
	/** Qualifier of the <code>Logistics.Order</code> attribute **/
	public static final String ORDER = "Order";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n ORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedLogistics> ORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedLogistics>(
	NagAccCoreConstants.TC.LOGISTICS,
	false,
	"Order",
	"OrderPOS",
	true,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(COMPANYNAME, AttributeMode.INITIAL);
		tmp.put(SUPPORTSBLAZINGFAST, AttributeMode.INITIAL);
		tmp.put(ACTIVEORDERS, AttributeMode.INITIAL);
		tmp.put(ORDERPOS, AttributeMode.INITIAL);
		tmp.put(ORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute.
	 * @return the activeOrders - activer orders
	 */
	public Integer getActiveOrders(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, ACTIVEORDERS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute.
	 * @return the activeOrders - activer orders
	 */
	public Integer getActiveOrders()
	{
		return getActiveOrders( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute. 
	 * @return the activeOrders - activer orders
	 */
	public int getActiveOrdersAsPrimitive(final SessionContext ctx)
	{
		Integer value = getActiveOrders( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute. 
	 * @return the activeOrders - activer orders
	 */
	public int getActiveOrdersAsPrimitive()
	{
		return getActiveOrdersAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - activer orders
	 */
	public void setActiveOrders(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, ACTIVEORDERS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - activer orders
	 */
	public void setActiveOrders(final Integer value)
	{
		setActiveOrders( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - activer orders
	 */
	public void setActiveOrders(final SessionContext ctx, final int value)
	{
		setActiveOrders( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - activer orders
	 */
	public void setActiveOrders(final int value)
	{
		setActiveOrders( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.code</code> attribute.
	 * @return the code - code of the logitics
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.code</code> attribute.
	 * @return the code - code of the logitics
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.code</code> attribute. 
	 * @param value the code - code of the logitics
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.code</code> attribute. 
	 * @param value the code - code of the logitics
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute.
	 * @return the companyName - company name of logistics
	 */
	public String getCompanyName(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedLogistics.getCompanyName requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, COMPANYNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute.
	 * @return the companyName - company name of logistics
	 */
	public String getCompanyName()
	{
		return getCompanyName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute. 
	 * @return the localized companyName - company name of logistics
	 */
	public Map<Language,String> getAllCompanyName(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,COMPANYNAME,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute. 
	 * @return the localized companyName - company name of logistics
	 */
	public Map<Language,String> getAllCompanyName()
	{
		return getAllCompanyName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - company name of logistics
	 */
	public void setCompanyName(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedLogistics.setCompanyName requires a session language", 0 );
		}
		setLocalizedProperty(ctx, COMPANYNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - company name of logistics
	 */
	public void setCompanyName(final String value)
	{
		setCompanyName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - company name of logistics
	 */
	public void setAllCompanyName(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,COMPANYNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - company name of logistics
	 */
	public void setAllCompanyName(final Map<Language,String> value)
	{
		setAllCompanyName( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		ORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.Order</code> attribute.
	 * @return the Order
	 */
	public Order getOrder(final SessionContext ctx)
	{
		return (Order)getProperty( ctx, ORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.Order</code> attribute.
	 * @return the Order
	 */
	public Order getOrder()
	{
		return getOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.Order</code> attribute. 
	 * @param value the Order
	 */
	public void setOrder(final SessionContext ctx, final Order value)
	{
		ORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.Order</code> attribute. 
	 * @param value the Order
	 */
	public void setOrder(final Order value)
	{
		setOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.OrderPOS</code> attribute.
	 * @return the OrderPOS
	 */
	 Integer getOrderPOS(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, ORDERPOS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.OrderPOS</code> attribute.
	 * @return the OrderPOS
	 */
	 Integer getOrderPOS()
	{
		return getOrderPOS( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.OrderPOS</code> attribute. 
	 * @return the OrderPOS
	 */
	 int getOrderPOSAsPrimitive(final SessionContext ctx)
	{
		Integer value = getOrderPOS( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.OrderPOS</code> attribute. 
	 * @return the OrderPOS
	 */
	 int getOrderPOSAsPrimitive()
	{
		return getOrderPOSAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.OrderPOS</code> attribute. 
	 * @param value the OrderPOS
	 */
	 void setOrderPOS(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, ORDERPOS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.OrderPOS</code> attribute. 
	 * @param value the OrderPOS
	 */
	 void setOrderPOS(final Integer value)
	{
		setOrderPOS( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.OrderPOS</code> attribute. 
	 * @param value the OrderPOS
	 */
	 void setOrderPOS(final SessionContext ctx, final int value)
	{
		setOrderPOS( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.OrderPOS</code> attribute. 
	 * @param value the OrderPOS
	 */
	 void setOrderPOS(final int value)
	{
		setOrderPOS( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute.
	 * @return the supportsBlazingFast - supports blazing fast delivery
	 */
	public Boolean isSupportsBlazingFast(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, SUPPORTSBLAZINGFAST);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute.
	 * @return the supportsBlazingFast - supports blazing fast delivery
	 */
	public Boolean isSupportsBlazingFast()
	{
		return isSupportsBlazingFast( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @return the supportsBlazingFast - supports blazing fast delivery
	 */
	public boolean isSupportsBlazingFastAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isSupportsBlazingFast( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @return the supportsBlazingFast - supports blazing fast delivery
	 */
	public boolean isSupportsBlazingFastAsPrimitive()
	{
		return isSupportsBlazingFastAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - supports blazing fast delivery
	 */
	public void setSupportsBlazingFast(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, SUPPORTSBLAZINGFAST,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - supports blazing fast delivery
	 */
	public void setSupportsBlazingFast(final Boolean value)
	{
		setSupportsBlazingFast( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - supports blazing fast delivery
	 */
	public void setSupportsBlazingFast(final SessionContext ctx, final boolean value)
	{
		setSupportsBlazingFast( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - supports blazing fast delivery
	 */
	public void setSupportsBlazingFast(final boolean value)
	{
		setSupportsBlazingFast( getSession().getSessionContext(), value );
	}
	
}
