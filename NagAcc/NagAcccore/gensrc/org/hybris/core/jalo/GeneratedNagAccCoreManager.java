/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 04-Jan-2022, 3:48:20 PM                     ---
 * ----------------------------------------------------------------
 */
package org.hybris.core.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.util.OneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hybris.NagAcc.core.jalo.BlazingFastProductCarouselComponent;
import org.hybris.core.constants.NagAccCoreConstants;
import org.hybris.core.jalo.ApparelProduct;
import org.hybris.core.jalo.ApparelSizeVariantProduct;
import org.hybris.core.jalo.ApparelStyleVariantProduct;
import org.hybris.core.jalo.ElectronicsColorVariantProduct;
import org.hybris.core.jalo.Logistics;

/**
 * Generated class for type <code>NagAccCoreManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedNagAccCoreManager extends Extension
{
	/**
	* {@link OneToManyHandler} for handling 1:n LOGISTICS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<Logistics> ORDER2LOGISTICSRELATIONLOGISTICSHANDLER = new OneToManyHandler<Logistics>(
	NagAccCoreConstants.TC.LOGISTICS,
	true,
	"Order",
	"OrderPOS",
	true,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("deliverySpeed", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.AbstractOrder", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("deliverySpeed", AttributeMode.INITIAL);
		tmp.put("logistics", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.product.Product", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	public ApparelProduct createApparelProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( NagAccCoreConstants.TC.APPARELPRODUCT );
			return (ApparelProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelProduct createApparelProduct(final Map attributeValues)
	{
		return createApparelProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( NagAccCoreConstants.TC.APPARELSIZEVARIANTPRODUCT );
			return (ApparelSizeVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelSizeVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final Map attributeValues)
	{
		return createApparelSizeVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( NagAccCoreConstants.TC.APPARELSTYLEVARIANTPRODUCT );
			return (ApparelStyleVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelStyleVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final Map attributeValues)
	{
		return createApparelStyleVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public BlazingFastProductCarouselComponent createBlazingFastProductCarouselComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( NagAccCoreConstants.TC.BLAZINGFASTPRODUCTCAROUSELCOMPONENT );
			return (BlazingFastProductCarouselComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating BlazingFastProductCarouselComponent : "+e.getMessage(), 0 );
		}
	}
	
	public BlazingFastProductCarouselComponent createBlazingFastProductCarouselComponent(final Map attributeValues)
	{
		return createBlazingFastProductCarouselComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( NagAccCoreConstants.TC.ELECTRONICSCOLORVARIANTPRODUCT );
			return (ElectronicsColorVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ElectronicsColorVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final Map attributeValues)
	{
		return createElectronicsColorVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public Logistics createLogistics(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( NagAccCoreConstants.TC.LOGISTICS );
			return (Logistics)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating Logistics : "+e.getMessage(), 0 );
		}
	}
	
	public Logistics createLogistics(final Map attributeValues)
	{
		return createLogistics( getSession().getSessionContext(), attributeValues );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.deliverySpeed</code> attribute.
	 * @return the deliverySpeed - Delivery Speed of the cart
	 */
	public EnumerationValue getDeliverySpeed(final SessionContext ctx, final AbstractOrder item)
	{
		return (EnumerationValue)item.getProperty( ctx, NagAccCoreConstants.Attributes.AbstractOrder.DELIVERYSPEED);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.deliverySpeed</code> attribute.
	 * @return the deliverySpeed - Delivery Speed of the cart
	 */
	public EnumerationValue getDeliverySpeed(final AbstractOrder item)
	{
		return getDeliverySpeed( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.deliverySpeed</code> attribute. 
	 * @param value the deliverySpeed - Delivery Speed of the cart
	 */
	public void setDeliverySpeed(final SessionContext ctx, final AbstractOrder item, final EnumerationValue value)
	{
		item.setProperty(ctx, NagAccCoreConstants.Attributes.AbstractOrder.DELIVERYSPEED,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.deliverySpeed</code> attribute. 
	 * @param value the deliverySpeed - Delivery Speed of the cart
	 */
	public void setDeliverySpeed(final AbstractOrder item, final EnumerationValue value)
	{
		setDeliverySpeed( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.deliverySpeed</code> attribute.
	 * @return the deliverySpeed - Delivery Speed of the product
	 */
	public EnumerationValue getDeliverySpeed(final SessionContext ctx, final Product item)
	{
		return (EnumerationValue)item.getProperty( ctx, NagAccCoreConstants.Attributes.Product.DELIVERYSPEED);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.deliverySpeed</code> attribute.
	 * @return the deliverySpeed - Delivery Speed of the product
	 */
	public EnumerationValue getDeliverySpeed(final Product item)
	{
		return getDeliverySpeed( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.deliverySpeed</code> attribute. 
	 * @param value the deliverySpeed - Delivery Speed of the product
	 */
	public void setDeliverySpeed(final SessionContext ctx, final Product item, final EnumerationValue value)
	{
		item.setProperty(ctx, NagAccCoreConstants.Attributes.Product.DELIVERYSPEED,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.deliverySpeed</code> attribute. 
	 * @param value the deliverySpeed - Delivery Speed of the product
	 */
	public void setDeliverySpeed(final Product item, final EnumerationValue value)
	{
		setDeliverySpeed( getSession().getSessionContext(), item, value );
	}
	
	@Override
	public String getName()
	{
		return NagAccCoreConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.logistics</code> attribute.
	 * @return the logistics - logistics details of the product
	 */
	public Logistics getLogistics(final SessionContext ctx, final Product item)
	{
		return (Logistics)item.getProperty( ctx, NagAccCoreConstants.Attributes.Product.LOGISTICS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.logistics</code> attribute.
	 * @return the logistics - logistics details of the product
	 */
	public Logistics getLogistics(final Product item)
	{
		return getLogistics( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.logistics</code> attribute. 
	 * @param value the logistics - logistics details of the product
	 */
	public void setLogistics(final SessionContext ctx, final Product item, final Logistics value)
	{
		item.setProperty(ctx, NagAccCoreConstants.Attributes.Product.LOGISTICS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.logistics</code> attribute. 
	 * @param value the logistics - logistics details of the product
	 */
	public void setLogistics(final Product item, final Logistics value)
	{
		setLogistics( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.Logistics</code> attribute.
	 * @return the Logistics
	 */
	public List<Logistics> getLogistics(final SessionContext ctx, final Order item)
	{
		return (List<Logistics>)ORDER2LOGISTICSRELATIONLOGISTICSHANDLER.getValues( ctx, item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.Logistics</code> attribute.
	 * @return the Logistics
	 */
	public List<Logistics> getLogistics(final Order item)
	{
		return getLogistics( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.Logistics</code> attribute. 
	 * @param value the Logistics
	 */
	public void setLogistics(final SessionContext ctx, final Order item, final List<Logistics> value)
	{
		ORDER2LOGISTICSRELATIONLOGISTICSHANDLER.setValues( ctx, item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.Logistics</code> attribute. 
	 * @param value the Logistics
	 */
	public void setLogistics(final Order item, final List<Logistics> value)
	{
		setLogistics( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to Logistics. 
	 * @param value the item to add to Logistics
	 */
	public void addToLogistics(final SessionContext ctx, final Order item, final Logistics value)
	{
		ORDER2LOGISTICSRELATIONLOGISTICSHANDLER.addValue( ctx, item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to Logistics. 
	 * @param value the item to add to Logistics
	 */
	public void addToLogistics(final Order item, final Logistics value)
	{
		addToLogistics( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from Logistics. 
	 * @param value the item to remove from Logistics
	 */
	public void removeFromLogistics(final SessionContext ctx, final Order item, final Logistics value)
	{
		ORDER2LOGISTICSRELATIONLOGISTICSHANDLER.removeValue( ctx, item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from Logistics. 
	 * @param value the item to remove from Logistics
	 */
	public void removeFromLogistics(final Order item, final Logistics value)
	{
		removeFromLogistics( getSession().getSessionContext(), item, value );
	}
	
}
