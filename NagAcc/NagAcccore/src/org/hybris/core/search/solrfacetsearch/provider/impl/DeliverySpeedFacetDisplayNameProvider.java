package org.hybris.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.hybris.core.enums.DeliverySpeed;
import org.hybris.core.enums.SwatchColorEnum;

import java.util.Locale;

public class DeliverySpeedFacetDisplayNameProvider extends AbstractFacetValueDisplayNameProvider {
    private EnumerationService enumerationService;
    private I18NService i18nService;
    private CommonI18NService commonI18NService;

    @Override
    public String getDisplayName(final SearchQuery query, final IndexedProperty property, final String facetValue) {
        if (facetValue == null)
        {
            return "";
        }

        final HybrisEnumValue deliverySpeedEnum = getEnumerationService().getEnumerationValue(DeliverySpeed.class, facetValue);

        Locale queryLocale = null;
        if (query == null || query.getLanguage() == null || query.getLanguage().isEmpty())
        {
            queryLocale = getI18nService().getCurrentLocale();
        }

        if (queryLocale == null && query != null)
        {
            queryLocale = getCommonI18NService().getLocaleForLanguage(getCommonI18NService().getLanguage(query.getLanguage()));
        }

        String deliverySpeed = getEnumerationService().getEnumerationName(deliverySpeedEnum, queryLocale);
        if (deliverySpeed == null || deliverySpeed.isEmpty())
        {
            deliverySpeed = facetValue;
        }

        return deliverySpeed;
    }

    public EnumerationService getEnumerationService() {
        return enumerationService;
    }

    public void setEnumerationService(EnumerationService enumerationService) {
        this.enumerationService = enumerationService;
    }

    public I18NService getI18nService() {
        return i18nService;
    }

    public void setI18nService(I18NService i18nService) {
        this.i18nService = i18nService;
    }

    public CommonI18NService getCommonI18NService() {
        return commonI18NService;
    }

    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }
}
