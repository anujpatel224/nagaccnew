package org.hybris.core.hooks;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.hooks.CartValidationHook;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.hybris.core.enums.DeliverySpeed;

import java.util.List;

public class DeliverySpeedValidator implements CartValidationHook {

    private ModelService modelService;
    private CartService cartService;
    @Override
    public void beforeValidateCart(CommerceCartParameter parameter, List<CommerceCartModification> modifications) {
        CartModel cartModel = parameter.getCart();
        List<AbstractOrderEntryModel> entryModelList = cartModel.getEntries();
        DeliverySpeed finalDeliverySpeed = null;
        Boolean hasRelaxedDeliverySpeed = false;
        Boolean hasStandardDeliverySpeed = false;
        Boolean hasExpressDeliverySpeed = false;
        for (AbstractOrderEntryModel entry : entryModelList) {

            if (entry.getProduct() != null && entry.getProduct().getDeliverySpeed() != null) {
                if (entry.getProduct().getDeliverySpeed().equals(DeliverySpeed.RELAXED)) {
                    hasRelaxedDeliverySpeed = true;
                    break;
                }

                if (entry.getProduct().getDeliverySpeed().equals(DeliverySpeed.STANDARD))
                    hasStandardDeliverySpeed = true;

                if (entry.getProduct().getDeliverySpeed().equals(DeliverySpeed.EXPRESS))
                    hasExpressDeliverySpeed = true;
            }

        }

        if (hasExpressDeliverySpeed)
            finalDeliverySpeed = DeliverySpeed.EXPRESS;
        if (hasStandardDeliverySpeed)
            finalDeliverySpeed = DeliverySpeed.STANDARD;
        if (hasRelaxedDeliverySpeed)
            finalDeliverySpeed = DeliverySpeed.RELAXED;
        cartModel = getCartService().getSessionCart();
        cartModel.setDeliverySpeed(finalDeliverySpeed);
        getModelService().save(cartModel);
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public CartService getCartService() {
        return cartService;
    }

    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @Override
    public void afterValidateCart(CommerceCartParameter parameter, List<CommerceCartModification> modifications) {

    }
}
