package org.hybris.core.job;

import com.hybris.yprofile.dto.Order;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import org.hybris.core.dao.CustomProductDAOImpl;
import org.hybris.core.model.LogisticsModel;
import org.hybris.core.model.OrderCountCronJobModel;
import org.hybris.core.services.CustomProductService;

import java.util.ArrayList;
import java.util.List;

public class OrderCountJob extends AbstractJobPerformable<OrderCountCronJobModel> {

    private CustomProductService customProductService;
    private ModelService modelService;
    @Override
    public PerformResult perform(OrderCountCronJobModel orderCountCronJobModel) {
        List<OrderModel> orders = customProductService.getAllOrders();
        List<LogisticsModel> logistics = customProductService.getAllLogistics();


        System.out.println("Executing Order cronjob.......");
        for (LogisticsModel logistic : logistics) {

            int count = 0;
            int previous_count = logistic.getActiveOrders();
            for (OrderModel orderModel : orders) {
                if (orderModel.getLogistics() != null && orderModel.getLogistics().contains(logistic)) {
                    count++;
                }
            }

            logistic.setActiveOrders(count);
            if(previous_count==count)
                System.out.println("No change in Logistic "+logistic.getCompanyName());
            else
                System.out.println("Updating active Orders in Logistic " + (logistic.getCompanyName()) + " from " + (previous_count) + " to " + count + " !");
            getModelService().save(logistic);


        }
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }


    public CustomProductService getCustomProductService() {
        return customProductService;
    }

    public void setCustomProductService(CustomProductService customProductService) {
        this.customProductService = customProductService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}
