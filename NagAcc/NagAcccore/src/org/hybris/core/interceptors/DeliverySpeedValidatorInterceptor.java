package org.hybris.core.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

public class DeliverySpeedValidatorInterceptor implements ValidateInterceptor<ProductModel> {
    @Override
    public void onValidate(ProductModel productModel, InterceptorContext interceptorContext) throws InterceptorException {
        Boolean throwError = false;

        String speedType = "";
        if (productModel.getDeliverySpeed() != null) {
            speedType = String.valueOf(productModel.getDeliverySpeed());
            if (speedType.equalsIgnoreCase("BlazingFast") && (productModel.getLogistics() == null || productModel.getLogistics().getSupportsBlazingFast() == false)) {
                throwError = true;
                throw new InterceptorException("Product must have Logistic and support blazing fast delivery service");

            }
        }
    }
}
