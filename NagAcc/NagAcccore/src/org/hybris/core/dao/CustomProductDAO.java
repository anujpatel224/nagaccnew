package org.hybris.core.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.hybris.core.model.LogisticsModel;

import java.util.List;

interface CustomProductDAO {
    public List<ProductModel> getProductWithBlazingFastSpeed();

    public List<OrderModel> getAllOrders();

    public  List<LogisticsModel> getALlLogistics();
}
