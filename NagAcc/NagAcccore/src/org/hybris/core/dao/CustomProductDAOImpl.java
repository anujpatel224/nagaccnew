package org.hybris.core.dao;

import de.hybris.platform.acceleratorservices.dataexport.googlelocal.model.Product;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.hybris.core.model.LogisticsModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomProductDAOImpl implements CustomProductDAO{
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<ProductModel> getProductWithBlazingFastSpeed() {
        StringBuilder query = new StringBuilder("Select {p.pk} From {Product as p Join EnumerationValue as enum on {enum.pk}={p.deliverySpeed}} Where {enum:code}='BlazingFast'");
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        final SearchResult searchResult = flexibleSearchService.search(searchQuery);
        List<ProductModel> productModelList = searchResult.getResult();
        return productModelList;
    }

    public List<OrderModel> getAllOrders()
    {
        StringBuilder query = new StringBuilder("SELECT {pk} FROM {Order} WHERE {creationTime} < NOW() - INTERVAL 1 DAY");
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        final SearchResult searchResult = flexibleSearchService.search(searchQuery);
        List<OrderModel> allOrders =searchResult.getResult();
        return allOrders;
    }

    public List<LogisticsModel> getALlLogistics()
    {
        StringBuilder query = new StringBuilder("SELECT {pk} FROM {Logistics}");
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        final SearchResult searchResult = flexibleSearchService.search(searchQuery);
        List<LogisticsModel> logisticsModels =searchResult.getResult();
        return logisticsModels;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
