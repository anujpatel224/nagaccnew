package org.hybris.core.services;

import de.hybris.platform.acceleratorservices.dataexport.googlelocal.model.Product;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.hybris.core.dao.CustomProductDAOImpl;
import org.hybris.core.model.LogisticsModel;

import java.util.List;

public class CustomProductService {
    private  CustomProductDAOImpl customProductDAO;

    public List<ProductModel> getProductWithBlazingFastSpeed()
    {
        return getCustomProductDAO().getProductWithBlazingFastSpeed();

    }

    public List<OrderModel> getAllOrders()
    {
        return getCustomProductDAO().getAllOrders();
    }

    public List<LogisticsModel> getAllLogistics() {
        return getCustomProductDAO().getALlLogistics();
    }
    public CustomProductDAOImpl getCustomProductDAO() {
        return customProductDAO;
    }

    public void setCustomProductDAO(CustomProductDAOImpl customProductDAO) {
        this.customProductDAO = customProductDAO;
    }
}
