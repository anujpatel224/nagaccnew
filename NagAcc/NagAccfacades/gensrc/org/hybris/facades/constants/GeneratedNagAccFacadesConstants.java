/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 04-Jan-2022, 3:12:52 PM                     ---
 * ----------------------------------------------------------------
 */
package org.hybris.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedNagAccFacadesConstants
{
	public static final String EXTENSIONNAME = "NagAccfacades";
	
	protected GeneratedNagAccFacadesConstants()
	{
		// private constructor
	}
	
	
}
