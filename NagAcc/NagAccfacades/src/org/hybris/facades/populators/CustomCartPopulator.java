package org.hybris.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.hybris.core.enums.DeliverySpeed;

import java.util.List;
public class CustomCartPopulator extends CartPopulator {

    private ModelService modelService;
    private CartService cartService;


    @Override
    public void populate(final CartModel source, final CartData target) {
        super.populate(source, target);
        DeliverySpeed finalDeliverySpeed = DeliverySpeed.BLAZINGFAST;
        List<AbstractOrderEntryModel> entries = source.getEntries();
        Boolean hasRelaxedDeliverySpeed = false;
        Boolean hasStandardDeliverySpeed = false;
        Boolean hasExpressDeliverySpeed = false;
        for (AbstractOrderEntryModel entry : entries) {

            if (entry.getProduct() != null && entry.getProduct().getDeliverySpeed() != null) {
                if (entry.getProduct().getDeliverySpeed().equals(DeliverySpeed.RELAXED)) {
                    hasRelaxedDeliverySpeed = true;
                    break;
                }

                if (entry.getProduct().getDeliverySpeed().equals(DeliverySpeed.STANDARD))
                    hasStandardDeliverySpeed = true;

                if (entry.getProduct().getDeliverySpeed().equals(DeliverySpeed.EXPRESS))
                    hasExpressDeliverySpeed = true;
            }

        }

        if (hasExpressDeliverySpeed)
            finalDeliverySpeed = DeliverySpeed.EXPRESS;
        if (hasStandardDeliverySpeed)
            finalDeliverySpeed = DeliverySpeed.STANDARD;
        if (hasRelaxedDeliverySpeed)
            finalDeliverySpeed = DeliverySpeed.RELAXED;
        target.setDeliverySpeed(finalDeliverySpeed.getCode());
        CartModel cartModel = getCartService().getSessionCart();
        cartModel.setDeliverySpeed(finalDeliverySpeed);
        getModelService().save(cartModel);
    }

    @Override
    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public CartService getCartService() {
        return cartService;
    }

    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }
}