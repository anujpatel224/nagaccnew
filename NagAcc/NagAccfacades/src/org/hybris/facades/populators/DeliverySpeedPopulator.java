package org.hybris.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Objects;

public class DeliverySpeedPopulator extends ProductBasicPopulator  {

    @Override
    public void populate(final ProductModel source, ProductData target) throws ConversionException {
        super.populate(source, target);

        if(Objects.nonNull(source.getDeliverySpeed()))
        {
            target.setDeliverySpeed(String.valueOf(source.getDeliverySpeed()));
        }



    }

}
