package org.hybris.facades.suggestion;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.searchservices.provider.impl.ProductAttributeSnIndexerValueProvider;
import de.hybris.platform.core.model.product.ProductModel;
import org.hybris.core.services.CustomProductService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomProductFacade {

    private CustomProductService customProductService;
    private ProductFacade productFacade;
    protected static final List<ProductOption> PRODUCT_OPTIONS= Arrays.asList(ProductOption.BASIC, ProductOption.PRICE);
    public List<ProductData> getProductWithBlazingFastSpeed()
    {
        List<ProductModel> productModelList =customProductService.getProductWithBlazingFastSpeed();
        final List <ProductData> productDataList= new ArrayList<>();
        for(final ProductModel pm: productModelList)
        {
            productDataList.add(getProductFacade().getProductForCodeAndOptions(pm.getCode(), PRODUCT_OPTIONS ));
        }
        return productDataList;
    }

    public CustomProductService getCustomProductService() {
        return customProductService;
    }

    public void setCustomProductService(CustomProductService customProductService) {
        this.customProductService = customProductService;
    }

    public ProductFacade getProductFacade() {
        return productFacade;
    }

    public void setProductFacade(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }
}
